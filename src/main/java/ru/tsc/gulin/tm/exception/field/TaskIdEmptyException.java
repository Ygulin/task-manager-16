package ru.tsc.gulin.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException{

    public TaskIdEmptyException() {
        super("Error! Task Id is empty...");
    }

}
