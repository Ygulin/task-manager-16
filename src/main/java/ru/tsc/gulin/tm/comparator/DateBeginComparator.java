package ru.tsc.gulin.tm.comparator;

import ru.tsc.gulin.tm.api.model.IHasDateBegin;
import java.util.Comparator;

public enum DateBeginComparator implements Comparator<IHasDateBegin> {

    INSTANCE;

    @Override
    public int compare(final IHasDateBegin o1, final IHasDateBegin o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateBegin() == null || o2.getDateBegin() == null) return 0;
        return o1.getDateBegin().compareTo(o2.getDateBegin());
    }

}
